import express from "express";
const app = express();
import bodyParser from "body-parser";
const PORT = APP.PORT;
import cors from "cors";
import mongoose from "mongoose";
import morgan from "morgan";
import dotenv from "dotenv";
import APP from "./config/app";
import DB from "./config/db";
import Post from "./routes/Post";

dotenv.config();

mongoose.Promise = global.Promise;
mongoose.connect(DB.URI, { useNewUrlParser: true }).then(
  () => {
    console.log("Database is connected");
  },
  err => {
    console.log("Can not connect to the database" + err);
  }
);

// options for cors midddleware
const options: cors.CorsOptions = {
  allowedHeaders: [
    "Origin",
    "X-Requested-With",
    "Content-Type",
    "Accept",
    "X-Access-Token"
  ],
  credentials: true,
  methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
  origin: [APP.URI, "*"],
  optionsSuccessStatus: 200,
  preflightContinue: false
};

// support application/json type post data
app.use(bodyParser.json());
// support application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));
// use cors middleware
app.use(cors(options));
app.use(morgan("combined"));

//enable pre-flight
app.options("*", cors(options));

app.use("/api/posts", Post);

app.listen(PORT, () => {
  console.log("Server is running on Port:", PORT);
});
