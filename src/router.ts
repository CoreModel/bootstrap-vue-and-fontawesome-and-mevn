import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Post from "./views/Dashboard/Post/Index.vue";
import PostCreate from "./views/Dashboard/Post/Create.vue";
import PostEdit from "./views/Dashboard/Post/Edit.vue";
import Login from "./views/Login.vue";
import Register from "./views/Register.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/register",
      name: "Register",
      component: Register
    },
    {
      path: "/dashboard/posts",
      name: "posts",
      component: Post
    },
    {
      path: "/dashboard/posts/create",
      name: "post-create",
      component: PostCreate
    },
    {
      path: "/dashboard/posts/:id/edit",
      name: "post-edit",
      component: PostEdit
    }
  ]
});
