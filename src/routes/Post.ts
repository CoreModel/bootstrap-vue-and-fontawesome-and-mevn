import express from "express";
const router = express.Router();

// Require Post model in our routes module
import Post from "../models/Post";

// Defined get data(index or listing) route
router.route("/").get((req, res) => {
  Post.find((err, posts) => {
    if (err) {
      res.json(err);
    } else {
      res.json(posts);
    }
  });
});

// Defined store route
router.route("/").post((req, res) => {
  const post = new Post(req.body);

  post
    .save()
    .then(() => {
      res.status(200).json({
        message: "Post successfully created"
      });
    })
    .catch(() => {
      res.status(400).send("Unable to create Post");
    });
});

// Defined edit route
router.route("/:id").get((req, res) => {
  const id = req.params.id;
  Post.findById(id, (err, post) => {
    if (err) {
      res.json(err);
    }
    res.json(post);
  });
});

//  Defined update route
router.route("/:id").post((req, res) => {
  Post.findById(req.params.id, (err, post) => {
    if (!post) res.status(404).send("data is not found");
    else {
      post.title = req.body.title;
      post.body = req.body.body;
      post
        .save()
        .then(() => {
          res.json("Post updated successfully");
        })
        .catch(() => {
          res.status(400).send("Unable to update Post");
        });
    }
  });
});

// Defined delete | remove | destroy route
router.route("/:id").delete((req, res) => {
  Post.findByIdAndRemove(
    {
      _id: req.params.id
    },
    (err, post) => {
      if (err) res.json(err);
      else res.json("Post successfully removed");
    }
  );
});

export default router;
