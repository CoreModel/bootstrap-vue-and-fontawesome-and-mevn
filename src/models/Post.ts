import mongoose from "mongoose";
import Schema = mongoose.Schema;
import Post from "../interfaces/Post";
import { Timestamp } from "bson";

// Define collection and schema for Post
const PostSchema = new Schema(
  {
    title: { type: String, required: true },
    body: { type: String, required: true }
  },
  {
    timestamps: true,
    collection: "posts"
  }
);

const Post = mongoose.model<Post & mongoose.Document>("Post", PostSchema);

export default Post;
