import dotenv from "dotenv";

dotenv.config();

const {
  DB_CONNECTION,
  DB_HOST,
  DB_PORT,
  DB_DATABASE,
  DB_USERNAME,
  DB_PASSWORD
} = process.env;

const DB = {
  URI:
    DB_CONNECTION +
    "://" +
    DB_USERNAME +
    ":" +
    DB_PASSWORD +
    "@" +
    DB_HOST +
    ":" +
    DB_PORT +
    "/" +
    DB_DATABASE +
    "?authSource=admin"
};

export default DB;
