import dotenv from "dotenv";

dotenv.config();

const { APP_URL, APP_PORT } = process.env;

const APP = {
  URI: APP_URL + ":" + APP_PORT,
  URL: APP_URL,
  PORT: APP_PORT
};

export default APP;
