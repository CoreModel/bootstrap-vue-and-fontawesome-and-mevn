## Application name to be changed, in package.json and src/db.ts as well

bootstrap-vue-and-fontawesome-and-mevn

## Application is based on this tutorial (up-to-date without the complete example with post):

https://appdividend.com/2018/11/21/mevn-stack-tutorial-with-example-from-scratch/

## MongoDB URI Connection options are:

https://docs.mongodb.com/manual/reference/connection-string/

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your server

```
yarn start
```

### Run your tests

```
yarn test
```

### Lints and fixes files

```
yarn lint
```

### Run your end-to-end tests

```
yarn test:e2e
```

### Run your unit tests

```
yarn test:unit
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
